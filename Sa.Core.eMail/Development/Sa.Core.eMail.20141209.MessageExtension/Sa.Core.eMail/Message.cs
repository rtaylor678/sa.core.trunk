﻿using System;
using System.Net.Mail;

namespace Sa.Core.eMail
{
	public class Message
	{
		public static string SendUnmonitored(string fromAddress, string fromName, string toAddress, string subject, string body, string locale = "en-US")
		{
			using (MailMessage mailMessage = new MailMessage())
			{
				const string senderAddress = "DoNotReply@solomononline.com";
				const string senderName = "Do Not Reply (Solomon Associates)";

				mailMessage.To.Add(new MailAddress(toAddress));

				mailMessage.Sender = new MailAddress(senderAddress, senderName);
				mailMessage.From = new MailAddress(fromAddress, fromName);

				mailMessage.Subject = subject;
				mailMessage.IsBodyHtml = false;

				mailMessage.Body = body + MessageText.FooterUnmonitored(locale);

				return Client.SendMessage(mailMessage);
			}
		}
	}
}