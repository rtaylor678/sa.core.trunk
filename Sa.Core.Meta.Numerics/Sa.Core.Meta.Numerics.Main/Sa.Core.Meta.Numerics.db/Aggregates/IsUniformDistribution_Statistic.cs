﻿using System;
using System.Data.SqlTypes;
using System.IO;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

[Serializable]
[SqlUserDefinedAggregate
	(
		Format.UserDefined,
		IsInvariantToNulls = true,
		IsInvariantToDuplicates = true,
		IsInvariantToOrder = true,
		IsNullIfEmpty = true,
		MaxByteSize = -1
	)
]
public class IsUniformDistribution_Statistic : MetaNumericsSample
{
	new public SqlDouble Terminate()
	{
		TestResult tr = base.sampleDataSet.KolmogorovSmirnovTest(new UniformDistribution());
		return tr.Statistic;
	}

	new public void Init()
	{
		base.Init();
	}

	new public void Accumulate(SqlDouble Observation)
	{
		base.Accumulate(Observation);
	}

	public void Merge(IsUniformDistribution_Statistic Group)
	{
		base.Merge(Group);
	}
}