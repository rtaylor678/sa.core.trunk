﻿using System;
using System.Data.SqlTypes;
using System.IO;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;

[Serializable]
public abstract class MetaNumericsSample : IBinarySerialize
{
	protected long observations;
	protected Sample sampleDataSet;

	protected virtual void Init()
	{
		this.observations = 0;
		this.sampleDataSet = new Sample();
	}

	protected virtual void Accumulate(SqlDouble Observation)
	{
		if (!(Observation.IsNull))
		{
			this.observations++;
			this.sampleDataSet.Add((double)Observation);
		}
	}

	protected virtual void Merge(MetaNumericsSample Group)
	{
		this.observations++;
		this.sampleDataSet.Add(Group.sampleDataSet);
	}

	protected virtual SqlDouble Terminate { get; set; }

	public void Read(BinaryReader r)
	{
		this.observations = r.ReadInt64();

		this.sampleDataSet = new Sample();

		for (int i = 0; i < this.observations; i++)
		{
			this.sampleDataSet.Add(r.ReadDouble());
		}
	}

	public void Write(BinaryWriter w)
	{
		w.Write(this.observations);

		foreach (double d in this.sampleDataSet)
		{
			w.Write(d);
		}
	}
}

[Serializable]
public abstract class MetaNumericsSampleProbability : IBinarySerialize
{
	protected long observations;
	protected Sample sampleDataSet;
	protected Double probability;

	protected virtual void Init()
	{
		this.observations = 0;
		this.sampleDataSet = new Sample();
	}

	protected virtual void Accumulate(SqlDouble Observations, SqlDouble Probability)
	{
		if (!(Observations.IsNull))
		{
			this.observations++;
			this.probability = (double)Probability;
			this.sampleDataSet.Add((double)Observations);
		}
	}

	protected virtual void Merge(MetaNumericsSampleProbability Group)
	{
		this.observations++;
		this.probability = (double)Group.probability;
		this.sampleDataSet.Add(Group.sampleDataSet);
	}

	protected virtual SqlDouble Terminate { get; set; }

	public void Read(BinaryReader r)
	{
		this.observations = r.ReadInt64();
		this.probability = r.ReadDouble();

		this.sampleDataSet = new Sample();

		for (int i = 0; i < this.observations; i++)
		{
			this.sampleDataSet.Add(r.ReadDouble());
		}
	}

	public void Write(BinaryWriter w)
	{
		w.Write(this.observations);
		w.Write(this.probability);

		foreach (double d in this.sampleDataSet)
		{
			w.Write(d);
		}
	}
}