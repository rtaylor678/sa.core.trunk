﻿using System;
using System.Data.SqlTypes;
using System.IO;

using Microsoft.SqlServer.Server;
using Meta.Numerics;

[Serializable]
[SqlUserDefinedAggregate
	(
		Format.UserDefined,
		IsInvariantToNulls = true,
		IsInvariantToDuplicates = true,
		IsInvariantToOrder = true,
		IsNullIfEmpty = true,
		MaxByteSize = -1
	)
]
public class ConfidenceInterval : MetaNumericsSampleProbability
{
	new public SqlDouble Terminate()
	{
		Meta.Numerics.Interval meanInverval = base.sampleDataSet.PopulationMean.ConfidenceInterval(base.probability);
		return meanInverval.Width / 2.0;
	}

	new public void Init()
	{
		base.Init();
	}

	new public void Accumulate(SqlDouble Value, SqlDouble Probability)
	{
		base.Accumulate(Value, Probability);
	}

	public void Merge(ConfidenceInterval Group)
	{
		base.Merge(Group);
	}
}