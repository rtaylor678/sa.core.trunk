﻿using System;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
	[SqlFunction]
	public static SqlDouble ChiSquared_Statistic(SqlInt32 DegreesOfFreedom, SqlDouble Probability)
	{
		Probability = (1.0 - Probability);
		ChiSquaredDistribution d = new ChiSquaredDistribution((int)DegreesOfFreedom);
		return d.InverseLeftProbability((double)Probability);
	}
}