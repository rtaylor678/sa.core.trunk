﻿using System;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
	[SqlFunction]
	public static SqlDouble ChiSquared_Probability(SqlInt32 DegreesOfFreedom, SqlDouble CStatistic)
	{
		ChiSquaredDistribution d = new ChiSquaredDistribution((int)DegreesOfFreedom);
		return (1.0 - d.LeftProbability((double)CStatistic));
	}
}