﻿CREATE TYPE [stat].[Results_Descriptive] AS TABLE
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics

	[qTile]				TINYINT				NOT	NULL	CHECK([qTile] >= 1),				--	Quartile value
	[SampleType]		VARCHAR(10)			NOT	NULL,

	[xCount]			SMALLINT			NOT	NULL	CHECK([xCount]	>= 1),				--	Item Count
	[xMin]				FLOAT				NOT	NULL,										--	Minimum
	[xMax]				FLOAT				NOT	NULL,										--	Maximum
	[xRange]			FLOAT				NOT	NULL,										--	Range

	[BreakPrevious]		FLOAT					NULL,
	[BreakNext]			FLOAT					NULL,

	[xMean]				FLOAT				NOT	NULL,										--	Mean								http://en.[w]ikipedia.org/wiki/Mean
	[xStDev]			FLOAT				NOT	NULL	CHECK([xStDev]	>= 0.0),			--	Standard Deviation			σ		http://en.[w]ikipedia.org/wiki/Standard_deviation
	[xVar]				FLOAT				NOT	NULL	CHECK([xVar]	>= 0.0),			--	Variance					σ²		http://en.[w]ikipedia.org/wiki/Variance

	[xErrorStandard]	FLOAT				NOT	NULL,										--	Standard Error						http://en.[w]ikipedia.org/wiki/Z-test
	[xCoeffVar]			FLOAT					NULL,										--  Coefficient of Variation			http://en.[w]ikipedia.org/wiki/Coefficient_of_variation
	[xIdxDisp]			FLOAT					NULL,										--  Index of Dispersion				http://en.[w]ikipedia.org/wiki/Variance-to-mean_ratio

	[xSum]				FLOAT				NOT	NULL,										--	Sum	of x					Σ(x)
	[xSumXiX]			FLOAT				NOT	NULL,										--	Sum of Error				Σ(xi-AVG(x))

	[wMean]				FLOAT					NULL,										--	Weighted Mean
	[xwDiv]				FLOAT					NULL,										--	SUM(x) / SUM(w)

	[xLimitLower]		FLOAT				NOT	NULL,										--	Minimum (Mean of smallest two items, >= @CapMin)
	[xLimitUpper]		FLOAT				NOT	NULL,										--	Minimum (Mean of largest two items, <= @CapMax)
	[xLimitRange]		FLOAT				NOT	NULL,

	[tTest]				FLOAT				NOT	NULL	CHECK([tTest]	>= 0.0),			--	Calcualted T Test
	[tConf]				FLOAT				NOT	NULL	CHECK([tConf]	>= 0.0),			--	Calculated Confidence
	[tAlpha]			FLOAT				NOT	NULL	CHECK([tAlpha]	>= 0.0),			--	Alpha' value						http://en.[w]ikipedia.org/wiki/Student's_t-test
	[tStat]				FLOAT				NOT	NULL	CHECK([tStat]	>= 0.0),			--	Student T Statistic
	[tHypo]				TINYINT				NOT	NULL,

	[iConfidence]		FLOAT				NOT	NULL	CHECK([iConfidence]	>= 0.0),		--	Confidence Interval					http://en.[w]ikipedia.org/wiki/Confidence_interval
	[iPrediction]		FLOAT				NOT	NULL	CHECK([iPrediction]	>= 0.0),		--	Prediction Interval					http://en.[w]ikipedia.org/wiki/Prediction_interval
	[iPredictionLower]	FLOAT				NOT	NULL,										--	Lower Prediction Limit
	[iConfidenceLower]	FLOAT				NOT	NULL,										--	Lower Confidence Limit
	[iConfidenceUpper]	FLOAT				NOT	NULL,										--	Upper Confidence Limit
	[iPredictionUpper]	FLOAT				NOT	NULL,										--	Upper Prediction Limit

	[BoxLowerExtreme]	FLOAT				NOT	NULL,
	[BoxLowerWhisker]	FLOAT				NOT	NULL,
	[BoxLower]			FLOAT				NOT	NULL,
	[BoxMedian]			FLOAT				NOT	NULL,
	[BoxUpper]			FLOAT				NOT	NULL,
	[BoxUpperWhisker]	FLOAT				NOT	NULL,
	[BoxUpperExtreme]	FLOAT				NOT	NULL,
	[BoxIQR]			FLOAT				NOT	NULL,
	[BoxWidth]			FLOAT				NOT	NULL,
	[BoxNotch]			FLOAT				NOT	NULL,

	[xKurtosis]			FLOAT					NULL,										--	Pearson's sample excess Kurtosis	http://en.[w]ikipedia.org/wiki/Kurtosis
	[xSkewness]			FLOAT					NULL,										--	Skewness (negative leans right)		http://en.[w]ikipedia.org/wiki/Skewness

	CHECK([xMin]			<= [xMean]),
	CHECK([xMean]			<= [xMax]),
	CHECK([xMin]			<= [xLimitLower]),
	CHECK([xMax]			>= [xLimitUpper]),
	CHECK([iConfidence]		<= [iPrediction]),

	CHECK([BoxLowerExtreme]	<= [BoxLowerWhisker]),
	CHECK([BoxLowerWhisker]	<= [BoxLower]),
	CHECK([BoxLower]		<= [BoxMedian]),
	CHECK([BoxMedian]		<= [BoxUpper]),
	CHECK([BoxUpper]		<= [BoxUpperWhisker]),
	CHECK([BoxUpperWhisker]	<= [BoxUpperExtreme]),

	PRIMARY KEY CLUSTERED([qTile] ASC)
);