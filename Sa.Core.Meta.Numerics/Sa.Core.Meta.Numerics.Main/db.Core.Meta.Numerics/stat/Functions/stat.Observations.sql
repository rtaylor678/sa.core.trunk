﻿CREATE FUNCTION [stat].[Observations]
(
	@SampleBasis		[stat].[Sample_RandomVar]		READONLY,							--	Data points to examine
	@SampleAudit		[stat].[Sample_RandomVar]		READONLY							--	Data points to examine
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[o].[Id],
	[o].[x],
	[o].[xPcntTile],
	[o].[xPcntRank],
	[o].[xCumeDist],
	[o].[xQuartile],
	[o].[xRank],
	[o].[xRankDense],
	[o].[zPcntTile],
	[o].[zScore],
	[o].[zQuartile],
	[o].[zHypothesis],
	[o].[ErrorAbsolute],
	[o].[ErrorRelative],
	[o].[ResidualStandard],
	[o].[ResidualAbsolute],
	[o].[ResidualRelative]
FROM
	[stat].[ObservationsAnalysis](@SampleBasis, @SampleAudit, DEFAULT, DEFAULT)	[o]
);