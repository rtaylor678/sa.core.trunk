﻿CREATE FUNCTION [stat].[Descriptive]
(
	@Sample				[stat].[Sample_RandomVar]		READONLY							--	Data points to examine
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[d].[qTile],
	[d].[SampleType],

	[d].[xCount],
	[d].[xMin],
	[d].[xMax],
	[d].[xRange],

	[d].[BreakPrevious],
	[d].[BreakNext],

	[d].[xMean],
	[d].[xStDev],
	[d].[xVar],

	[d].[xErrorStandard],
	[d].[xCoeffVar],
	[d].[xIdxDisp],

	[d].[xSum],
	[d].[xSumXiX],

	[d].[wMean],
	[d].[xwDiv],

	[d].[xLimitLower],
	[d].[xLimitUpper],
	[d].[xLimitRange],

	[d].[tTest],
	[d].[tConf],
	[d].[tAlpha],
	[d].[tStat],
	[d].[tHypo],

	[d].[iConfidence],
	[d].[iPrediction],
	[d].[iPredictionLower],
	[d].[iConfidenceLower],
	[d].[iConfidenceUpper],
	[d].[iPredictionUpper],

	[d].[BoxLowerExtreme],
	[d].[BoxLowerWhisker],
	[d].[BoxLower],
	[d].[BoxMedian],
	[d].[BoxUpper],
	[d].[BoxUpperWhisker],
	[d].[BoxUpperExtreme],
	[d].[BoxIQR],
	[d].[BoxWidth],
	[d].[BoxNotch],

	[d].[xKurtosis],
	[d].[xSkewness]
FROM
	[stat].[DescriptiveArithmetic](@Sample, 0.90, 1, 1, NULL, NULL, 2) [d]
);