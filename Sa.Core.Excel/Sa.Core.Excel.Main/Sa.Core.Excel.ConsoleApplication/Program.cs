﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	class Program
	{
		static void Main_LoopingItems(string[] args)
		{
			const string extension = "*.xls";
			Regex reg = new Regex("OSIM[0-9][0-9][0-9][0-9]SEEC[0-9][0-9][0-9]" + extension);
			const string pthTarget = "C:\\ExcelTesting\\OSIM_YYYY_SEEC_NNN_NoCode.xls";

			List<string> Years = new List<string>();
			//Years.Add("2010");
			//Years.Add("2011");
			//Years.Add("2012");
			Years.Add("2013");

			string folder;
			string pthSaveAs;

			Excel.Application xla = SaExcel.NewExcelApplication(false);

			foreach (string Year in Years)
			{
				folder = "\\\\Dallas2\\data\\Data\\STUDY\\SEEC\\" + Year + "\\Correspondence";

				List<string> filePaths = Directory.GetFiles(folder, extension, SearchOption.AllDirectories)
						.Where(path => reg.IsMatch(path))
						.ToList();

				foreach (string pthSource in filePaths)
				{
					Console.WriteLine(Path.GetFileNameWithoutExtension(pthSource));

					pthSaveAs = Path.GetDirectoryName(pthSource) + "\\" + Path.GetFileNameWithoutExtension(pthSource) + "_VI" + Path.GetExtension(pthSource);

					Excel.Workbook wkbSource = SaExcel.OpenWorkbook_ReadOnly(xla, pthSource);
					Excel.Workbook wkbTarget = SaExcel.OpenWorkbook_ReadOnly(xla, pthTarget);

					SaExcel.CopyRangeValues(wkbSource, wkbTarget, rangeSEEC());
					SaExcel.ProtectWorkbookAll(wkbTarget, "saipassword");

					wkbTarget.KeepChangeHistory = false;
					wkbTarget.SaveAs(pthSaveAs);

					SaExcel.CloseWorkbook(ref wkbSource);
					SaExcel.CloseWorkbook(ref wkbTarget);
				}
			}
			SaExcel.CloseApplication(ref xla);
		}

		static void Main_copyWorkbookData()
		{
			const string folderRoot = "C:\\Users\\RRH\\Desktop\\Osim 2011";
			const string extension = "*.XLS";
			Regex reg = new Regex("OSIM[0-9][0-9][0-9][0-9]PCH[0-9][0-9][0-9]_VI" + extension);

			const string pthTarget = "C:\\Users\\RRH\\Desktop\\OSIM2013 Template.xls";

			List<string> filePaths = Directory.GetFiles(folderRoot, extension, SearchOption.AllDirectories)
					.Where(path => reg.IsMatch(path))
					.ToList();

			string pthSaveAs;

			Excel.Application xla = SaExcel.NewExcelApplication(false);

			foreach (string pthSource in filePaths)
			{
				Console.WriteLine(Path.GetFileNameWithoutExtension(pthSource));
				pthSaveAs = Path.GetDirectoryName(pthSource) + "\\" + Path.GetFileNameWithoutExtension(pthSource).Replace("_VI", "_Rollover").Replace("2011", "2013") + Path.GetExtension(pthSource).ToLower();

				Excel.Workbook wkbSource = SaExcel.OpenWorkbook_ReadOnly(xla, pthSource);
				Excel.Workbook wkbTarget = SaExcel.OpenWorkbook_ReadOnly(xla, pthTarget);

				SaExcel.UnProtectWorkbookAll(wkbSource, "saipassword");
				SaExcel.UnProtectWorkbookAll(wkbTarget, "saipassword");

				SaExcel.CopyRangeValues(wkbSource, wkbTarget, rangeOSIM());
				SaExcel.ProtectWorkbookAll(wkbTarget, "saipassword");

				wkbTarget.KeepChangeHistory = false;
				wkbTarget.SaveAs(pthSaveAs);

				SaExcel.CloseWorkbook(ref wkbSource);
				SaExcel.CloseWorkbook(ref wkbTarget);
			}

			SaExcel.CloseApplication(ref xla);
		}

		static void Main()
		{ 
			string dirSource = "\\\\Dallas2\\data\\Data\\STUDY\\Olefins\\2011\\Correspondence\\";
			string dirTarget = "\\\\Dallas2\\data\\Data\\STUDY\\Olefins\\2013\\Correspondence\\";

			string pthSource;
			string pthTarget;
			string pthSaveAs;

			List<string[]> FilesList = new List<string[]>();

			FilesList.Add(new string[2] { "11PCH109\\OSIM2011PCH109.xls", "13PCH109R\\OSIM2013PCH109R.xls" });
			FilesList.Add(new string[2] { "11PCH134\\OSIM2011PCH134.xls", "13PCH134R\\OSIM2013PCH134R.xls" });
			FilesList.Add(new string[2] { "11PCH176\\OSIM2011PCH176.xls", "13PCH176R\\OSIM2013PCH176R.xls" });
			FilesList.Add(new string[2] { "11PCH186\\OSIM2011PCH186.xls", "13PCH186R\\OSIM2013PCH186R.xls" });
			FilesList.Add(new string[2] { "11PCH201\\OSIM2011PCH201.xls", "13PCH201R\\OSIM2013PCH201R.xls" });

			Excel.Application xla = SaExcel.NewExcelApplication(true);

			foreach (string[] s in FilesList)
			{
				pthSource = dirSource + s[0];
				pthTarget = dirTarget + s[1];

				pthSaveAs = Path.GetDirectoryName(pthTarget) + "\\" + Path.GetFileNameWithoutExtension(pthTarget) + " Table6-4 Edits" + Path.GetExtension(pthTarget).ToLower();

				Excel.Workbook wkbSource = SaExcel.OpenWorkbook_ReadOnly(xla, pthSource);
				Excel.Workbook wkbTarget = SaExcel.OpenWorkbook_ReadOnly(xla, pthTarget);

				SaExcel.UnProtectWorkbookAll(wkbTarget, "saipassword");

				SaExcel.CopyRangeValues(wkbSource, wkbTarget, rangeOSIM_Table6_4());

				SaExcel.WorkbookSaveCopyAs(wkbTarget, pthSaveAs);

				SaExcel.CloseWorkbook(ref wkbSource);
				SaExcel.CloseWorkbook(ref wkbTarget);
			}
			SaExcel.CloseApplication(ref xla);
		}

		static List<string[]> rangeOSIM_Table6_4()
		{
			List<string[]> _range = new List<string[]>();

			_range.Add(new string[2] { "Table6-4", "G6:AH6" });
			_range.Add(new string[2] { "Table6-4", "G8:AH11" });
			_range.Add(new string[2] { "Table6-4", "G13:AH13" });
			_range.Add(new string[2] { "Table6-4", "G24:AH25" });
			_range.Add(new string[2] { "Table6-4", "G32:AH37" });
			_range.Add(new string[2] { "Table6-4", "G39:AH40" });
			_range.Add(new string[2] { "Table6-4", "G43:AH45" });

			return _range;
		}

		static List<string[]> rangeOSIM()
		{
			List<string[]> _range = new List<string[]>();

			_range.Add(new string[2] { "PlantName", "D6:D7" });
			_range.Add(new string[2] { "PlantName", "D12:D26" });

			_range.Add(new string[2] { "Table1-1", "F10:F11" });
			_range.Add(new string[2] { "Table1-1", "F13:F14" });
			_range.Add(new string[2] { "Table1-1", "H10:I12" });
			_range.Add(new string[2] { "Table1-1", "I19:I29" });

			_range.Add(new string[2] { "Table1-2", "G11:I22" });
			_range.Add(new string[2] { "Table1-2", "G31:H33" });

			_range.Add(new string[2] { "Table1-3", "G6:G27" });
			_range.Add(new string[2] { "Table1-3", "I6" });
			_range.Add(new string[2] { "Table1-3", "I12:I28" });
			_range.Add(new string[2] { "Table1-3", "J7" });
			_range.Add(new string[2] { "Table1-3", "K6:K28" });

			_range.Add(new string[2] { "Table1-3", "F34" });
			_range.Add(new string[2] { "Table1-3", "F41:F43" });
			_range.Add(new string[2] { "Table1-3", "F46:F54" });
			_range.Add(new string[2] { "Table1-3", "G32:G33" });
			_range.Add(new string[2] { "Table1-3", "G35:G40" });
			_range.Add(new string[2] { "Table1-3", "G44:G45" });
			_range.Add(new string[2] { "Table1-3", "H32:H45" });
			_range.Add(new string[2] { "Table1-3", "J32:J45" });
			_range.Add(new string[2] { "Table1-3", "B57:E59" });
			_range.Add(new string[2] { "Table1-3", "G57:G59" });

			_range.Add(new string[2] { "Table1-3", "F66:G66" });
			_range.Add(new string[2] { "Table1-3", "H64" });
			_range.Add(new string[2] { "Table1-3", "H67" });
			_range.Add(new string[2] { "Table1-3", "I68" });
			_range.Add(new string[2] { "Table1-3", "J64" });

			_range.Add(new string[2] { "Table2A-1", "H4" });
			_range.Add(new string[2] { "Table2A-1", "C6:H24" });
			_range.Add(new string[2] { "Table2A-1", "C27:H29" });
			_range.Add(new string[2] { "Table2A-1", "C31:H34" });
			_range.Add(new string[2] { "Table2A-1", "C36:H39" });
			_range.Add(new string[2] { "Table2A-1", "C43:H43" });
			_range.Add(new string[2] { "Table2A-1", "C45:H45" });
			_range.Add(new string[2] { "Table2A-1", "H46" });

			_range.Add(new string[2] { "Table2A-2", "L4:N4" });
			_range.Add(new string[2] { "Table2A-2", "C5:N6" });
			_range.Add(new string[2] { "Table2A-2", "C8:N17" });
			_range.Add(new string[2] { "Table2A-2", "C20:N24" });
			_range.Add(new string[2] { "Table2A-2", "C26:N26" });
			_range.Add(new string[2] { "Table2A-2", "C28:N30" });
			_range.Add(new string[2] { "Table2A-2", "C32:N35" });
			_range.Add(new string[2] { "Table2A-2", "C37:N40" });
			_range.Add(new string[2] { "Table2A-2", "C44:N44" });
			_range.Add(new string[2] { "Table2A-2", "C46:N46" });
			_range.Add(new string[2] { "Table2A-2", "L47:N47" });

			_range.Add(new string[2] { "Table2B", "C6:E24" });
			_range.Add(new string[2] { "Table2B", "C27:E29" });
			_range.Add(new string[2] { "Table2B", "C31:E34" });
			_range.Add(new string[2] { "Table2B", "C36:E39" });
			_range.Add(new string[2] { "Table2B", "C43:E43" });
			_range.Add(new string[2] { "Table2B", "C45:E45" });

			_range.Add(new string[2] { "Table2C", "C10:F38" });
			_range.Add(new string[2] { "Table2C", "H10:H34" });
			_range.Add(new string[2] { "Table2C", "B38" });
			_range.Add(new string[2] { "Table2C", "C44:C46" });
			_range.Add(new string[2] { "Table2C", "C53:C56" });

			_range.Add(new string[2] { "Table3", "D9:G38" });
			_range.Add(new string[2] { "Table3", "I9:I18" });
			_range.Add(new string[2] { "Table3", "I32:I35" });
			_range.Add(new string[2] { "Table3", "C37:C38" });
			_range.Add(new string[2] { "Table3", "I37:I38" });
			_range.Add(new string[2] { "Table3", "D40:G42" });
			_range.Add(new string[2] { "Table3", "F52:F65" });
			_range.Add(new string[2] { "Table3", "I52:I65" });

			_range.Add(new string[2] { "Check3", "I27:J27" });
			_range.Add(new string[2] { "Check3", "I37:J39" });
			_range.Add(new string[2] { "Check3", "K17:L17" });

			_range.Add(new string[2] { "Table4", "F7:F22" });
			_range.Add(new string[2] { "Table4", "F26:F26" });
			_range.Add(new string[2] { "Table4", "F29:F46" });
			_range.Add(new string[2] { "Table4", "F50:F53" });
			_range.Add(new string[2] { "Table4", "D56" });
			_range.Add(new string[2] { "Table4", "E60:E67" });
			_range.Add(new string[2] { "Table4", "C73:C81" });
			_range.Add(new string[2] { "Table4", "E73:E80" });
			_range.Add(new string[2] { "Table4", "C86:C94" });
			_range.Add(new string[2] { "Table4", "E86:E93" });

			_range.Add(new string[2] { "Table5A", "D10:G17" });
			_range.Add(new string[2] { "Table5A", "D21:G27" });
			_range.Add(new string[2] { "Table5A", "D31:G36" });
			_range.Add(new string[2] { "Table5A", "F42" });

			_range.Add(new string[2] { "Table5B", "D9:F9" });
			_range.Add(new string[2] { "Table5B", "D12:F16" });
			_range.Add(new string[2] { "Table5B", "D19:F19" });
			_range.Add(new string[2] { "Table5B", "D22:F28" });
			_range.Add(new string[2] { "Table5B", "D31:F35" });
			_range.Add(new string[2] { "Table5B", "D38:F39" });
			_range.Add(new string[2] { "Table5B", "E44" });

			_range.Add(new string[2] { "Table6-1", "F10:I43" });
			_range.Add(new string[2] { "Table6-1", "B47:I50" });
			_range.Add(new string[2] { "Table6-1", "B54:I57" });
			_range.Add(new string[2] { "Table6-1", "F72:I75" });
			_range.Add(new string[2] { "Table6-1", "B83:I86" });

			_range.Add(new string[2] { "Table6-2", "E11:E19" });
			_range.Add(new string[2] { "Table6-2", "C19" });
			_range.Add(new string[2] { "Table6-2", "E26" });
			_range.Add(new string[2] { "Table6-2", "F42" });
			_range.Add(new string[2] { "Table6-2", "F46" });

			_range.Add(new string[2] { "Table6-3", "G6:H6" });
			_range.Add(new string[2] { "Table6-3", "F7:H11" });
			_range.Add(new string[2] { "Table6-3", "F15:H15" });
			_range.Add(new string[2] { "Table6-3", "F18:H18" });
			_range.Add(new string[2] { "Table6-3", "F20" });
			_range.Add(new string[2] { "Table6-3", "F25:I25" });
			_range.Add(new string[2] { "Table6-3", "H30" });
			_range.Add(new string[2] { "Table6-3", "H34" });
			_range.Add(new string[2] { "Table6-3", "H37" });

			_range.Add(new string[2] { "Table6-4", "C6:AH6" });
			_range.Add(new string[2] { "Table6-4", "C8:AH11" });
			_range.Add(new string[2] { "Table6-4", "C13:AH13" });
			_range.Add(new string[2] { "Table6-4", "C24:AH25" });
			_range.Add(new string[2] { "Table6-4", "C32:AH37" });
			_range.Add(new string[2] { "Table6-4", "C39:AH40" });
			_range.Add(new string[2] { "Table6-4", "C43:AH45" });
			_range.Add(new string[2] { "Table6-4", "C47" });

			_range.Add(new string[2] { "Table6-4", "AK49:AQ50" });
			_range.Add(new string[2] { "Table6-4", "AJ51:AJ52" });
			_range.Add(new string[2] { "Table6-4", "AL51:AQ52" });
			_range.Add(new string[2] { "Table6-4", "AJ53:AK53" });
			_range.Add(new string[2] { "Table6-4", "AM53:AQ53" });
			_range.Add(new string[2] { "Table6-4", "AJ54:AP54" });
			_range.Add(new string[2] { "Table6-4", "AJ55:AL60" });
			_range.Add(new string[2] { "Table6-4", "AN55:AQ60" });
			_range.Add(new string[2] { "Table6-4", "AJ61:AM61" });
			_range.Add(new string[2] { "Table6-4", "AO61:AQ61" });
			_range.Add(new string[2] { "Table6-4", "AJ62:AN63" });
			_range.Add(new string[2] { "Table6-4", "AP62:AQ63" });
			_range.Add(new string[2] { "Table6-4", "AJ64:AO66" });
			_range.Add(new string[2] { "Table6-4", "AQ64:AQ66" });
			_range.Add(new string[2] { "Table6-4", "AJ67:AQ70" });

			_range.Add(new string[2] { "Check6", "I13" });
			_range.Add(new string[2] { "Check6", "E45" });

			_range.Add(new string[2] { "Table7", "F7:H11" });
			_range.Add(new string[2] { "Table7", "G12:H14" });
			_range.Add(new string[2] { "Table7", "D16:H19" });
			_range.Add(new string[2] { "Table7", "F21" });
			_range.Add(new string[2] { "Table7", "D24" });
			_range.Add(new string[2] { "Table7", "H24" });
			_range.Add(new string[2] { "Table7", "G27:G33" });

			_range.Add(new string[2] { "Table7", "D40:H43" });
			_range.Add(new string[2] { "Table7", "D45" });
			_range.Add(new string[2] { "Table7", "F45" });
			_range.Add(new string[2] { "Table7", "H45" });
			_range.Add(new string[2] { "Table7", "H53" });

			_range.Add(new string[2] { "Table7", "F57:H60" });
			_range.Add(new string[2] { "Table7", "C60" });

			_range.Add(new string[2] { "Table7", "H63" });
			_range.Add(new string[2] { "Table7", "H65" });
			_range.Add(new string[2] { "Table7", "H68" });

			_range.Add(new string[2] { "Table7", "Q13" });
			_range.Add(new string[2] { "Table7", "Q17" });
			_range.Add(new string[2] { "Table7", "Q24" });

			_range.Add(new string[2] { "Check7", "F27" });

			_range.Add(new string[2] { "Table8-1", "G15:H17" });
			_range.Add(new string[2] { "Table8-1", "G19:H20" });
			_range.Add(new string[2] { "Table8-1", "H29" });
			_range.Add(new string[2] { "Table8-1", "H32" });
			_range.Add(new string[2] { "Table8-1", "H35" });
			_range.Add(new string[2] { "Table8-1", "H39:H45" });
			_range.Add(new string[2] { "Table8-1", "C41" });
			_range.Add(new string[2] { "Table8-1", "H51" });
			_range.Add(new string[2] { "Table8-1", "H53:H55" });
			_range.Add(new string[2] { "Table8-1", "H58" });
			_range.Add(new string[2] { "Table8-1", "H62" });

			_range.Add(new string[2] { "Table8-2", "I7:I20" });
			_range.Add(new string[2] { "Table8-2", "H27:H58" });

			_range.Add(new string[2] { "Table8-3", "G21:H26" });
			_range.Add(new string[2] { "Table8-3", "G39:H46" });
			_range.Add(new string[2] { "Table8-3", "I39" });
			_range.Add(new string[2] { "Table8-3", "I41" });
			_range.Add(new string[2] { "Table8-3", "H54" });

			_range.Add(new string[2] { "Table8-4", "E10:E64" });

			_range.Add(new string[2] { "Table9-1", "F33:F37" });

			_range.Add(new string[2] { "Table9-2", "J9:J44" });
			_range.Add(new string[2] { "Table9-2", "I21" });
			_range.Add(new string[2] { "Table9-2", "I48" });
			_range.Add(new string[2] { "Table9-2", "I50" });

			_range.Add(new string[2] { "Table9-3", "I6:I38" });
			_range.Add(new string[2] { "Table9-3", "I44:I52" });

			_range.Add(new string[2] { "Table9-4", "I10:I30" });
			_range.Add(new string[2] { "Table9-4", "H42:J53" });

			_range.Add(new string[2] { "Table9-5", "H11:J53" });

			_range.Add(new string[2] { "Table9-6", "H10:J63" });

			_range.Add(new string[2] { "Table10-1", "E33:H52" });
			_range.Add(new string[2] { "Table10-1", "G54:H54" });
			_range.Add(new string[2] { "Table10-1", "E56:H56" });
			_range.Add(new string[2] { "Table10-1", "D63" });

			_range.Add(new string[2] { "Table10-2", "G15:G22" });
			_range.Add(new string[2] { "Table10-2", "I36:I42" });

			_range.Add(new string[2] { "Table11", "K43:K46" });
			_range.Add(new string[2] { "Table11", "H51:K57" });
			_range.Add(new string[2] { "Table11", "I62:K65" });
			_range.Add(new string[2] { "Table11", "H67:H68" });
			_range.Add(new string[2] { "Table11", "E77:H80" });
			_range.Add(new string[2] { "Table11", "E77:H80" });
			_range.Add(new string[2] { "Table11", "G91:G105" });
			_range.Add(new string[2] { "Table11", "H91:G102" });
			_range.Add(new string[2] { "Table11", "F109:G109" });
			_range.Add(new string[2] { "Table11", "G116:H116" });
			_range.Add(new string[2] { "Table11", "G118" });
			_range.Add(new string[2] { "Table11", "J126:K126" });
			_range.Add(new string[2] { "Table11", "G130:H135" });
			_range.Add(new string[2] { "Table11", "F139:K151" });
			_range.Add(new string[2] { "Table11", "F153" });
			_range.Add(new string[2] { "Table11", "J156:J166" });
			_range.Add(new string[2] { "Table11", "U109:AB126" });

			_range.Add(new string[2] { "Table12-1", "D6:I16" });
			_range.Add(new string[2] { "Table12-1", "D18:I20" });
			_range.Add(new string[2] { "Table12-1", "D23:I30" });

			_range.Add(new string[2] { "Table12-2", "F7:K21" });
			_range.Add(new string[2] { "Table12-2", "C16:E18" });
			_range.Add(new string[2] { "Table12-2", "F28:K64" });
			_range.Add(new string[2] { "Table12-2", "C66:K70" });
			_range.Add(new string[2] { "Table12-2", "F76:K76" });

			_range.Add(new string[2] { "Table13", "F15:I24" });
			_range.Add(new string[2] { "Table13", "F31:I34" });
			_range.Add(new string[2] { "Table13", "F46:I47" });
			_range.Add(new string[2] { "Table13", "F53:I53" });
			_range.Add(new string[2] { "Table13", "H59:H61" });

			return _range;
		}

		static List<string[]> rangeSEEC()
		{
			List<string[]> _range = new List<string[]>();

			_range.Add(new string[2] { "PlantName", "D6:D7" });
			_range.Add(new string[2] { "PlantName", "D12:D22" });
			_range.Add(new string[2] { "PlantName", "D25:D26" });
			_range.Add(new string[2] { "PlantName", "C40:C40" });

			_range.Add(new string[2] { "Table1-1", "F10:F11" });
			_range.Add(new string[2] { "Table1-1", "F13:F14" });
			_range.Add(new string[2] { "Table1-1", "H10:I12" });
			_range.Add(new string[2] { "Table1-1", "I19:I29" });

			_range.Add(new string[2] { "Table1-3", "G6:G27" });
			_range.Add(new string[2] { "Table1-3", "I6" });
			_range.Add(new string[2] { "Table1-3", "I12:I28" });
			_range.Add(new string[2] { "Table1-3", "J7" });
			_range.Add(new string[2] { "Table1-3", "K6:K28" });

			_range.Add(new string[2] { "Table1-3", "F34" });
			_range.Add(new string[2] { "Table1-3", "F54" });
			_range.Add(new string[2] { "Table1-3", "G32:G33" });
			_range.Add(new string[2] { "Table1-3", "G35:G45" });
			_range.Add(new string[2] { "Table1-3", "H32:H45" });
			_range.Add(new string[2] { "Table1-3", "J32:J45" });
			_range.Add(new string[2] { "Table1-3", "B57:E59" });
			_range.Add(new string[2] { "Table1-3", "G57:G59" });

			_range.Add(new string[2] { "Table1-3", "H64" });
			_range.Add(new string[2] { "Table1-3", "J64" });
			_range.Add(new string[2] { "Table1-3", "F66:G66" });
			_range.Add(new string[2] { "Table1-3", "H67" });
			_range.Add(new string[2] { "Table1-3", "I68" });

			_range.Add(new string[2] { "Table2A-1", "H4" });
			_range.Add(new string[2] { "Table2A-1", "C6:H24" });
			_range.Add(new string[2] { "Table2A-1", "C27:H29" });
			_range.Add(new string[2] { "Table2A-1", "C31:H34" });
			_range.Add(new string[2] { "Table2A-1", "C36:H39" });
			_range.Add(new string[2] { "Table2A-1", "C43:H43" });
			_range.Add(new string[2] { "Table2A-1", "C45:H45" });

			_range.Add(new string[2] { "Table2A-2", "L4:N4" });
			_range.Add(new string[2] { "Table2A-2", "C5:N6" });
			_range.Add(new string[2] { "Table2A-2", "C8:N17" });
			_range.Add(new string[2] { "Table2A-2", "C20:N24" });
			_range.Add(new string[2] { "Table2A-2", "C26:N26" });
			_range.Add(new string[2] { "Table2A-2", "C28:N30" });
			_range.Add(new string[2] { "Table2A-2", "C32:N35" });
			_range.Add(new string[2] { "Table2A-2", "C37:N40" });
			_range.Add(new string[2] { "Table2A-2", "C44:N44" });
			_range.Add(new string[2] { "Table2A-2", "C46:N46" });

			_range.Add(new string[2] { "Table2B", "C6:E24" });
			_range.Add(new string[2] { "Table2B", "C27:E29" });
			_range.Add(new string[2] { "Table2B", "C31:E34" });
			_range.Add(new string[2] { "Table2B", "C36:E39" });
			_range.Add(new string[2] { "Table2B", "C43:E43" });
			_range.Add(new string[2] { "Table2B", "C45:E45" });

			_range.Add(new string[2] { "Table2C", "C10:F38" });
			_range.Add(new string[2] { "Table2C", "H10:H34" });
			_range.Add(new string[2] { "Table2C", "B38" });
			_range.Add(new string[2] { "Table2C", "C44:C46" });
			_range.Add(new string[2] { "Table2C", "C53:C56" });

			_range.Add(new string[2] { "Table3", "D9:G38" });
			_range.Add(new string[2] { "Table3", "I9:I35" });
			_range.Add(new string[2] { "Table3", "C37:G38" });
			_range.Add(new string[2] { "Table3", "D40:G42" });
			_range.Add(new string[2] { "Table3", "F52:F65" });
			_range.Add(new string[2] { "Table3", "I52:I65" });

			_range.Add(new string[2] { "Check6", "I13" });
			_range.Add(new string[2] { "Check6", "E45" });

			_range.Add(new string[2] { "Table7", "F7:F11" });
			_range.Add(new string[2] { "Table7", "G7:G14" });
			_range.Add(new string[2] { "Table7", "D16:G19" });
			_range.Add(new string[2] { "Table7", "F21" });
			_range.Add(new string[2] { "Table7", "D24" });
			_range.Add(new string[2] { "Table7", "G27:G33" });
			_range.Add(new string[2] { "Table7", "D40:G43" });
			_range.Add(new string[2] { "Table7", "D45" });
			_range.Add(new string[2] { "Table7", "F45" });
			_range.Add(new string[2] { "Table7", "H53" });
			_range.Add(new string[2] { "Table7", "H63" });
			_range.Add(new string[2] { "Table7", "H65" });
			_range.Add(new string[2] { "Table7", "H68" });
			_range.Add(new string[2] { "Table7", "D73:E76" });
			_range.Add(new string[2] { "Table7", "D78:E81" });
			_range.Add(new string[2] { "Table7", "D83:E86" });
			_range.Add(new string[2] { "Table7", "G78:G81" });
			_range.Add(new string[2] { "Table7", "F90:G96" });

			_range.Add(new string[2] { "Table7", "Q13" });
			_range.Add(new string[2] { "Table7", "Q17" });
			_range.Add(new string[2] { "Table7", "Q24" });

			return _range;
		}
	}
}