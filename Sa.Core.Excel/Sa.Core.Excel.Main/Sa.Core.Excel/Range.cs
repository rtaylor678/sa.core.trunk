﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	public partial class XL
	{
		public static bool RangeHasValue(Excel.Range rng)
		{
			string s = string.Empty;

			foreach (Excel.Range r in rng.Cells)
			{
				if (r.Value != null)
				{
					s = Convert.ToString(r.Text).Trim();

					if (s.Length > 0) s = Convert.ToString(r.Value).Trim();
					if (s.Length > 0) return true;
				}
			}
			return false;
		}

		public static void CopyRangeValues(Excel.Workbook wkbSource, Excel.Workbook wkbTarget, List<string[]> range)
		{
			Excel.Worksheet wksSource;
			Excel.Worksheet wksTarget;

			Excel.Range rngSource;
			Excel.Range rngTarget;

			foreach (string[] r in range)
			{
				wksSource = wkbSource.Worksheets[r[0]];
				rngSource = wksSource.Range[r[1]];

				wksTarget = wkbTarget.Worksheets[r[0]];
				rngTarget = wksTarget.Range[r[1]];

				XL.CopyRangeValues(rngSource, rngTarget);
			}

			wkbTarget.Parent.CalculateFullRebuild();
		}

		public static void CopyRangeValues(Excel.Range source, Excel.Range target)
		{
			target.Value = source.Value;
		}

		public static string Address(Excel.Range rng)
		{
			return rng.AddressLocal.Replace("$", "");
		}
	}
}