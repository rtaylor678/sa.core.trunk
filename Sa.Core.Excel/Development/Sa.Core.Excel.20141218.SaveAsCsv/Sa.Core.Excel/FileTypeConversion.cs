﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	public partial class SaExcel
	{
		public static void WorkbookSaveAsCsv(Excel.Workbook wkb, string path)
		{
			wkb.SaveAs(path, Excel.XlFileFormat.xlCSV, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, false, Type.Missing, Type.Missing, Type.Missing);
		}
	}
}
