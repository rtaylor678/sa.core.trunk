﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	public partial class SaExcel
	{
		public static void ProtectWorkbook(Excel.Workbook wkb, string password)
		{
			wkb.Protect(password);
		}

		public static void ProtectWorksheet(Excel.Worksheet wks, string password)
		{
			wks.Protect(password);
		}

		public static void ProtectWorkbookAll(Excel.Workbook wkb, string password)
		{
			foreach (Excel.Worksheet wks in wkb.Worksheets)
			{
				ProtectWorksheet(wks, password);
			}

			ProtectWorkbook(wkb, password);
		}

		public static void UnProtectWorkbook(Excel.Workbook wkb, string password)
		{
			wkb.Unprotect(password);
		}

		public static void UnProtectWorksheet(Excel.Worksheet wks, string password)
		{
			wks.Unprotect(password);
		}

		public static void UnProtectWorkbookAll(Excel.Workbook wkb, string password)
		{
			UnProtectWorkbook(wkb, password);

			foreach (Excel.Worksheet wks in wkb.Worksheets)
			{
				UnProtectWorksheet(wks, password);
			}
		}
	}
}
