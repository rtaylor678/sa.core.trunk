﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	class Program
	{
		static void Main(string[] args)
		{
			const string extension = "*.xls";
			Regex reg = new Regex("OSIM[0-9][0-9][0-9][0-9]SEEC[0-9][0-9][0-9]" + extension);
			const string pthTarget = "C:\\ExcelTesting\\OSIM_YYYY_SEEC_NNN_NoCode.xls";

			List<string> Years = new List<string>();
			Years.Add("2009");
			Years.Add("2010");
			Years.Add("2011");
			Years.Add("2012");
			Years.Add("2013");

			string folder;
			string pthSaveAs;

			Excel.Application xla = SaExcel.NewExcelApplication(false);

			foreach (string Year in Years)
			{
				folder = "\\\\Dallas2\\data\\Data\\STUDY\\SEEC\\" + Year + "\\Correspondence\\" + Year.Substring(2, 2) + "SEEC227\\";

				List<string> filePaths = Directory.GetFiles(folder, extension, SearchOption.AllDirectories)
						.Where(path => reg.IsMatch(path))
						.ToList();

				foreach (string pthSource in filePaths)
				{
					Console.WriteLine(Path.GetFileNameWithoutExtension(pthSource));

					pthSaveAs = Path.GetDirectoryName(pthSource) + "\\" + Path.GetFileNameWithoutExtension(pthSource) + "_VI" + Path.GetExtension(pthSource);

					Excel.Workbook wkbSource = SaExcel.OpenWorkbook_ReadOnly(xla, pthSource);
					Excel.Workbook wkbTarget = SaExcel.OpenWorkbook_ReadOnly(xla, pthTarget);

					SaExcel.CopyRangeValues(wkbSource, wkbTarget, rangeSet());
					SaExcel.ProtectWorkbookAll(wkbTarget, "saipassword");

					wkbTarget.KeepChangeHistory = false;
					wkbTarget.SaveAs(pthSaveAs);

					SaExcel.CloseWorkbook(ref wkbSource);
					SaExcel.CloseWorkbook(ref wkbTarget);
				}
			}
			SaExcel.CloseApplication(ref xla);
		}

		static List<string[]> rangeSet()
		{
			List<string[]> _range = new List<string[]>();

			_range.Add(new string[2] { "PlantName", "D6:D7" });
			_range.Add(new string[2] { "PlantName", "D12:D22" });
			_range.Add(new string[2] { "PlantName", "D25:D26" });
			_range.Add(new string[2] { "PlantName", "C40:C40" });

			_range.Add(new string[2] { "Table1-1", "F10:F11" });
			_range.Add(new string[2] { "Table1-1", "F13:F14" });
			_range.Add(new string[2] { "Table1-1", "H10:I12" });
			_range.Add(new string[2] { "Table1-1", "I19:I29" });

			_range.Add(new string[2] { "Table1-3", "G6:G27" });
			_range.Add(new string[2] { "Table1-3", "I6" });
			_range.Add(new string[2] { "Table1-3", "I12:I28" });
			_range.Add(new string[2] { "Table1-3", "J7" });
			_range.Add(new string[2] { "Table1-3", "K6:K28" });

			_range.Add(new string[2] { "Table1-3", "F34" });
			_range.Add(new string[2] { "Table1-3", "F54" });
			_range.Add(new string[2] { "Table1-3", "G32:G33" });
			_range.Add(new string[2] { "Table1-3", "G35:G45" });
			_range.Add(new string[2] { "Table1-3", "H32:H45" });
			_range.Add(new string[2] { "Table1-3", "J32:J45" });
			_range.Add(new string[2] { "Table1-3", "B57:E59" });
			_range.Add(new string[2] { "Table1-3", "G57:G59" });

			_range.Add(new string[2] { "Table1-3", "H649" });
			_range.Add(new string[2] { "Table1-3", "J64" });
			_range.Add(new string[2] { "Table1-3", "F66:G66" });
			_range.Add(new string[2] { "Table1-3", "H67" });
			_range.Add(new string[2] { "Table1-3", "I68" });

			_range.Add(new string[2] { "Table2A-1", "H4" });
			_range.Add(new string[2] { "Table2A-1", "C6:H24" });
			_range.Add(new string[2] { "Table2A-1", "C27:H29" });
			_range.Add(new string[2] { "Table2A-1", "C31:H34" });
			_range.Add(new string[2] { "Table2A-1", "C36:H39" });
			_range.Add(new string[2] { "Table2A-1", "C43:H43" });
			_range.Add(new string[2] { "Table2A-1", "C45:H45" });

			_range.Add(new string[2] { "Table2A-2", "L4:N4" });
			_range.Add(new string[2] { "Table2A-1", "C5:N6" });
			_range.Add(new string[2] { "Table2A-1", "C8:N17" });
			_range.Add(new string[2] { "Table2A-1", "C20:N24" });
			_range.Add(new string[2] { "Table2A-1", "C26:N26" });
			_range.Add(new string[2] { "Table2A-1", "C28:N30" });
			_range.Add(new string[2] { "Table2A-1", "C32:N35" });
			_range.Add(new string[2] { "Table2A-1", "C37:N40" });
			_range.Add(new string[2] { "Table2A-1", "C44:N44" });
			_range.Add(new string[2] { "Table2A-1", "C46:N46" });

			_range.Add(new string[2] { "Table2B", "C6:E24" });
			_range.Add(new string[2] { "Table2B", "C27:E29" });
			_range.Add(new string[2] { "Table2B", "C31:E34" });
			_range.Add(new string[2] { "Table2B", "C36:E39" });
			_range.Add(new string[2] { "Table2B", "C43:E43" });
			_range.Add(new string[2] { "Table2B", "C45:E45" });

			_range.Add(new string[2] { "Table2C", "C10:F38" });
			_range.Add(new string[2] { "Table2C", "H10:H34" });
			_range.Add(new string[2] { "Table2C", "B26" });
			_range.Add(new string[2] { "Table2C", "C44:C46" });
			_range.Add(new string[2] { "Table2C", "C53:C56" });

			_range.Add(new string[2] { "Table3", "D9:G38" });
			_range.Add(new string[2] { "Table3", "I9:I35" });
			_range.Add(new string[2] { "Table3", "C37:G38" });
			_range.Add(new string[2] { "Table3", "D40:G42" });
			_range.Add(new string[2] { "Table3", "F52:F65" });
			_range.Add(new string[2] { "Table3", "I52:I65" });

			_range.Add(new string[2] { "Check6", "I13" });

			_range.Add(new string[2] { "Table7", "F7:F11" });
			_range.Add(new string[2] { "Table7", "G7:G14" });
			_range.Add(new string[2] { "Table7", "D16:G19" });
			_range.Add(new string[2] { "Table7", "F21" });
			_range.Add(new string[2] { "Table7", "D24" });
			_range.Add(new string[2] { "Table7", "G27:G33" });
			_range.Add(new string[2] { "Table7", "D40:G43" });
			_range.Add(new string[2] { "Table7", "D45" });
			_range.Add(new string[2] { "Table7", "F45" });
			_range.Add(new string[2] { "Table7", "H53" });
			_range.Add(new string[2] { "Table7", "H63" });
			_range.Add(new string[2] { "Table7", "H65" });
			_range.Add(new string[2] { "Table7", "H68" });
			_range.Add(new string[2] { "Table7", "D73:E76" });
			_range.Add(new string[2] { "Table7", "D78:E81" });
			_range.Add(new string[2] { "Table7", "D83:E86" });
			_range.Add(new string[2] { "Table7", "G78:G81" });
			_range.Add(new string[2] { "Table7", "F90:G96" });

			_range.Add(new string[2] { "Table7", "Q13" });
			_range.Add(new string[2] { "Table7", "Q17" });
			_range.Add(new string[2] { "Table7", "Q24" });

			return _range;
		}

	}
}
