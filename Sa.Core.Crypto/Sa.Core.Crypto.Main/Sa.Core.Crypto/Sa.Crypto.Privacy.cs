﻿using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;

namespace Sa.Core
{
	public static partial class Crypto
	{
		/// <summary>
		/// Addresses a message to a specific recipient, increaing confidence that only the indended recipient can read the message.
		/// </summary>
		public static class Privacy
		{
			/// <summary>
			/// Signs and encodes a message for a specific recipient using a public certificate.
			/// </summary>
			/// <param name="message"></param>
			/// <param name="certPublic">Public certificate corresponding to the recipient private certificte.</param>
			/// <param name="envelope"></param>
			/// <returns></returns>
			public static bool Encode(byte[] message, X509Certificate2 certPublic, out byte[] envelope)
			{
				try
				{
					ContentInfo contentInfo = new ContentInfo(message);
					EnvelopedCms envelopedCms = new EnvelopedCms(contentInfo);
					CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.IssuerAndSerialNumber, certPublic);
					envelopedCms.Encrypt(recipient);
					envelope = envelopedCms.Encode();
					return true;
				}
				catch
				{
					envelope = new byte[0];
					return false;
				}
			}

			/// <summary>
			/// Verifies and decodes a message.
			/// The recipients private certificate must be installed on the computer
			/// </summary>
			/// <param name="envelope"></param>
			/// <param name="message"></param>
			/// <returns></returns>
			public static bool Decode(byte[] envelope, out byte[] message)
			{
				try
				{
					EnvelopedCms envelopedCms = new EnvelopedCms();
					envelopedCms.Decode(envelope);
					envelopedCms.Decrypt();
					message = envelopedCms.Encode();
					return true;
				}
				catch
				{
					message = new byte[0];
					return false;
				}
			}
		}
	}
}