﻿using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Sa.Core
{
	/// <summary>
	/// Performs the encryption, decryption, and verification of a byte array.
	/// The byte array is signed during the encryption process. During decryption, the signature is verified.
	/// </summary>
	public static partial class Crypto
	{
		/// <summary>
		/// The message is not readable
		/// </summary>
		public static class Confidentiality
		{
			public static class Symmetric
			{
				private const int AesKeySize = 256;
				private const int AesBlockSize = 128;
				private const int AesFeedbackSize = 128;

				private const CipherMode AesCipherMode = CipherMode.CBC;
				private const PaddingMode AesPaddingMode = PaddingMode.ISO10126;

				public static bool Encrypt(byte[] plainText, byte[] encryptionKey, out byte[] aesCipher)
				{
					bool encryptBytes = false;

					try
					{
						using (AesManaged aes = new AesManaged())
						{
							aes.BlockSize = AesBlockSize;
							aes.FeedbackSize = AesFeedbackSize;
							aes.KeySize = AesKeySize;
							aes.Mode = AesCipherMode;
							aes.Padding = AesPaddingMode;

							aes.Key = encryptionKey;

							using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
							{
								rng.GetBytes(aes.IV);
							}

							using (MemoryStream ms = new MemoryStream())
							{
								using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
								{
									cs.Write(plainText, 0, plainText.Length);
									cs.Flush();
									cs.Close();
								}
								aesCipher = aes.IV.Concat(ms.ToArray()).ToArray();
								encryptBytes = true;
								ms.Flush();
								ms.Close();
							}
							aes.Clear();
						}
					}
					catch
					{
						aesCipher = new byte[0];
						encryptBytes = false;
					}
					return encryptBytes;
				}

				public static bool Decrypt(byte[] aesCipher, byte[] encryptionKey, out byte[] plainText)
				{
					bool decryptBytes = false;

					try
					{
						using (AesManaged aes = new AesManaged())
						{
							aes.BlockSize = AesBlockSize;
							aes.FeedbackSize = AesFeedbackSize;
							aes.KeySize = AesKeySize;
							aes.Mode = AesCipherMode;
							aes.Padding = AesPaddingMode;

							aes.IV = Bytes.Left(aesCipher, aes.IV.Length, out aesCipher);
							aes.Key = encryptionKey;

							using (MemoryStream ms = new MemoryStream())
							{
								using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
								{
									cs.Write(aesCipher, 0, aesCipher.Length);
									cs.Flush();
									cs.Close();
								}
								plainText = ms.ToArray();
								decryptBytes = true;
								ms.Flush();
								ms.Close();
							}
							aes.Clear();
						}
					}
					catch
					{
						plainText = new byte[0];
						decryptBytes = false;
					}
					return decryptBytes;
				}
			}

			public static class Asymmetric
			{
				public static void Encrypt(byte[] plainText, X509Certificate2 certPublic, out byte[] rsaCipher)
				{
					using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)certPublic.PublicKey.Key)
					{
						rsaCipher = rsa.Encrypt(plainText, true);
					}
				}

				public static void Decrypt(byte[] rsaCipher, X509Certificate2 certPrivate, out byte[] plainText)
				{
					using (RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)certPrivate.PrivateKey)
					{
						plainText = rsa.Decrypt(rsaCipher, true);
					}
				}
			}
		}
	}
}