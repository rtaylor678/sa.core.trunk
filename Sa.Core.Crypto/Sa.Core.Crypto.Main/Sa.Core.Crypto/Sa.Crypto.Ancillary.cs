﻿using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;

namespace Sa.Core
{
	/// <summary>
	/// Performs the encryption, decryption, and verification of a byte array.
	/// The byte array is signed during the encryption process. During decryption, the signature is verified.
	/// </summary>
	public static partial class Crypto
	{
		//	thought: include asymetrically encrypted
		//		confKey
		//		IV
		//		hmacKey
		//		signature
		//	along with 'message' rather than appending and splitting
		//	the keys can be ramdom with each message
		//	and certificates can be verified
		//public class Package
		//{
		//	byte[] confKey;
		//	byte[] hmacMessage;
		//	byte[] hmacConfidentiality;

		//	byte[] IV;
		//	byte[] signatureMessage;
		//	byte[] signatureConfidentiality;

		//	byte[] message;
		//}

		public static class Password
		{
			public static bool Derive(byte[] password, byte[] salt, int length, out byte[] key)
			{
				const int RfcIterations = 3001;
				
				if (salt.Length >= 8)
				{
					using (Rfc2898DeriveBytes rfcKey = new Rfc2898DeriveBytes(password, salt, RfcIterations))
					{
						key = rfcKey.GetBytes(length);
						return true;
					}
				}
				else
				{
					key = new byte[0];
					return false;
				}
			}

			public static bool Generate(int length, out byte[] randomNumber)
			{
				randomNumber = new byte[length];

				using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
				{
					rng.GetBytes(randomNumber);
				}

				return true;
			}
		}

		public static class Certificate
		{
			public static bool Get(string name, out X509Certificate2 certificate)
			{
				try
				{
					using (X509Store storeMy = new X509Store(StoreName.My, StoreLocation.CurrentUser))
					{
						storeMy.Open(OpenFlags.ReadOnly);

						X509Certificate2Collection certColl = storeMy.Certificates.Find(X509FindType.FindBySubjectName, name, true);
						certificate = certColl[0];

						storeMy.Close();
						certColl.Clear();
						return true;
					}
				}
				catch
				{
					certificate = new X509Certificate2();
					return false;
				}
			}
		}

		public static class Bytes
		{
			public static byte[] Left(byte[] raw, int length, out byte[] right)
			{
				byte[] l;
				Split(raw, length, out l, out right);
				return l;
			}

			public static bool Split(byte[] raw, int length, out byte[] left, out byte[] right)
			{
				left = raw.Take(length).ToArray();
				right = raw.Skip(length).Take(raw.Length - length).ToArray();
				return true;
			}
		}
	}
}