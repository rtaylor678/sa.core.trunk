﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Core
{
	public partial class Crypto
	{
		[TestMethod]
		public void Password()
		{
			int length = 256;

			byte[] passwordA;
			byte[] passwordB;

			Sa.Core.Crypto.Password.Generate(length, out passwordA);
			Sa.Core.Crypto.Password.Generate(length, out passwordB);

			Assert.IsFalse(passwordA.SequenceEqual(passwordB));

			Assert.IsTrue(passwordA.Length == length);
			Assert.IsTrue(passwordB.Length == length);
			Assert.IsTrue(passwordA.Length == passwordB.Length);
		}
	}
}