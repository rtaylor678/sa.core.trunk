﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Core
{
	public partial class Crypto
	{
		[TestMethod]
		public void Compression()
		{
			byte[] regBytes = new byte[100];

			byte[] cmpBytes;
			Sa.Core.GZip.Compress(regBytes, out cmpBytes);

			byte[] expBytes;
			Sa.Core.GZip.Expand(cmpBytes, out expBytes);

			Assert.IsTrue(regBytes.SequenceEqual(expBytes));
			Assert.IsTrue(regBytes.Length >= cmpBytes.Length);
			Assert.IsTrue(expBytes.Length >= cmpBytes.Length);
		}
	}
}