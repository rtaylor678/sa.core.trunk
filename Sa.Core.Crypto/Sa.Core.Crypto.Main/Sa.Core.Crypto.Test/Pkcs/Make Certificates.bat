SETLOCAL ENABLEEXTENSIONS
rem https://msdn.microsoft.com/en-us/library/bfsktky3(v=vs.110).aspx
rem http://www.jayway.com/2014/09/03/creating-self-signed-certificates-with-makecert-exe-for-development/


rem install the *.pfx file on the client (the computer connecting to solomon) - this is the private key identifying the client computer
rem install the *.cer file on the server (Trusted People Store) - this is the public key that will validate the identity of the client computer.

rem Sa.ClientName.Sig.Host		ac5a49ff-19ce-436b-a653-f240db70 edf6
rem Sa.ClientName.Enc.Client	f858d286-9b52-4d2c-a616-4390a4b2 b2c3
rem Sa.ClientName.Sig.Client	8227d82d-b4a1-4f8d-8b2b-88f3f957 c8d3
rem Sa.ClientName.Enc.Host		4bf00370-3146-404d-97c3-15e0c75a 8c29

rem Create the private key (*pvk) and the public key (*.cer)
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\makecert.exe -r -pe -a sha512 -len 4096 ^
-n "CN=Sa.ClientName.Sig.Host" ^
-b 01/01/2015 -e 12/31/2015 ^
-cy authority ^
-sv "Sa.ClientName.Sig.Host.pvk" ^
"Sa.ClientName.Sig.Host.cer"

rem Create the Key Pair
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\pvk2pfx.exe -pvk "Sa.ClientName.Sig.Host.pvk" -spc "Sa.ClientName.Sig.Host.cer" -pfx "Sa.ClientName.Sig.Host.pfx" -pi "ac5a49ff-19ce-436b-a653-f240db70" -f

rem Create the SNK
C:\"Program Files (x86)"\"Microsoft SDKs"\Windows\v8.0A\bin\"NETFX 4.0 Tools"\sn.exe -p "Sa.ClientName.Sig.Host.pfx" "Sa.ClientName.Sig.Host.snk"

rem Create the private key (*pvk) and the public key (*.cer)
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\makecert.exe -r -pe -a sha512 -len 4096 ^
-n "CN=Sa.ClientName.Enc.Client" ^
-b 01/01/2015 -e 12/31/2015 ^
-cy authority ^
-sv "Sa.ClientName.Enc.Client.pvk" ^
"Sa.ClientName.Enc.Client.cer"

rem Create the Key Pair
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\pvk2pfx.exe -pvk "Sa.ClientName.Enc.Client.pvk" -spc "Sa.ClientName.Enc.Client.cer" -pfx "Sa.ClientName.Enc.Client.pfx" -pi "f858d286-9b52-4d2c-a616-4390a4b2" -f

rem Create the SNK
C:\"Program Files (x86)"\"Microsoft SDKs"\Windows\v8.0A\bin\"NETFX 4.0 Tools"\sn.exe -p "Sa.ClientName.Enc.Client.pfx" "Sa.ClientName.Enc.Client.snk"

rem Create the private key (*pvk) and the public key (*.cer)
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\makecert.exe -r -pe -a sha512 -len 4096 ^
-n "CN=Sa.ClientName.Sig.Client" ^
-b 01/01/2015 -e 12/31/2015 ^
-cy authority ^
-sv "Sa.ClientName.Sig.Client.pvk" ^
"Sa.ClientName.Sig.Client.cer"

rem Create the Key Pair
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\pvk2pfx.exe -pvk "Sa.ClientName.Sig.Client.pvk" -spc "Sa.ClientName.Sig.Client.cer" -pfx "Sa.ClientName.Sig.Client.pfx" -pi "8227d82d-b4a1-4f8d-8b2b-88f3f957" -f

rem Create the SNK
C:\"Program Files (x86)"\"Microsoft SDKs"\Windows\v8.0A\bin\"NETFX 4.0 Tools"\sn.exe -p "Sa.ClientName.Sig.Client.pfx" "Sa.ClientName.Sig.Client.snk"

rem Create the private key (*pvk) and the public key (*.cer)
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\makecert.exe -r -pe -a sha512 -len 4096 ^
-n "CN=Sa.ClientName.Enc.Host" ^
-b 01/01/2015 -e 12/31/2015 ^
-cy authority ^
-sv "Sa.ClientName.Enc.Host.pvk" ^
"Sa.ClientName.Enc.Host.cer"

rem Create the Key Pair
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\pvk2pfx.exe -pvk "Sa.ClientName.Enc.Host.pvk" -spc "Sa.ClientName.Enc.Host.cer" -pfx "Sa.ClientName.Enc.Host.pfx" -pi "4bf00370-3146-404d-97c3-15e0c75a" -f

rem Create the SNK
C:\"Program Files (x86)"\"Microsoft SDKs"\Windows\v8.0A\bin\"NETFX 4.0 Tools"\sn.exe -p "Sa.ClientName.Enc.Host.pfx" "Sa.ClientName.Enc.Host.snk"


