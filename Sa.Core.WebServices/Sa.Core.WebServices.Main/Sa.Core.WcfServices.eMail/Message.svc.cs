﻿namespace Sa.Core.WcfService.eMail
{
	public class Message : IMessage
	{
		public string SendFromUnmonitored(string fromAddress, string fromName, string toAddress, string subject, string body, string locale = "en-US")
		{
			return Sa.Core.eMail.Message.SendFromUnmonitored(fromAddress, fromName, toAddress, subject, body, locale);
		}
	}
}
