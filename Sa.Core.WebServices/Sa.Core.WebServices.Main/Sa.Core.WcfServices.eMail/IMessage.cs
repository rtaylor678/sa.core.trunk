﻿using System.ServiceModel;

namespace Sa.Core.WcfService.eMail
{
	[ServiceContract]
	public interface IMessage
	{
		[OperationContract]
		string SendFromUnmonitored(string fromAddress, string fromName, string toAddress, string subject, string body, string locale = "en-US");
	}
}
